/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.wrapp.floatlabelededittext;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int fletBackground = 0x7f0300eb;
        public static final int fletPadding = 0x7f0300ec;
        public static final int fletPaddingBottom = 0x7f0300ed;
        public static final int fletPaddingLeft = 0x7f0300ee;
        public static final int fletPaddingRight = 0x7f0300ef;
        public static final int fletPaddingTop = 0x7f0300f0;
        public static final int fletTextAppearance = 0x7f0300f1;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] FloatLabeledEditText = { 0x7f0300eb, 0x7f0300ec, 0x7f0300ed, 0x7f0300ee, 0x7f0300ef, 0x7f0300f0, 0x7f0300f1 };
        public static final int FloatLabeledEditText_fletBackground = 0;
        public static final int FloatLabeledEditText_fletPadding = 1;
        public static final int FloatLabeledEditText_fletPaddingBottom = 2;
        public static final int FloatLabeledEditText_fletPaddingLeft = 3;
        public static final int FloatLabeledEditText_fletPaddingRight = 4;
        public static final int FloatLabeledEditText_fletPaddingTop = 5;
        public static final int FloatLabeledEditText_fletTextAppearance = 6;
    }
}
