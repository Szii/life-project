package com.delaroystudios.alarmreminder.alarm;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import com.delaroystudios.alarmreminder.database.ListTypeConverters;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;

@SuppressWarnings("unchecked")
public class AlarmDao_Impl implements AlarmDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfAlarmModel;

  private final ListTypeConverters __listTypeConverters = new ListTypeConverters();

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfAlarmModel;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfAlarmModel;

  public AlarmDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfAlarmModel = new EntityInsertionAdapter<AlarmModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `AlarmModel`(`id`,`alertLabel`,`repeatDays`,`dateAlert`,`alertTime`,`attachedPhoto`,`isKeepAlert`) VALUES (?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, AlarmModel value) {
        stmt.bindLong(1, value.id);
        if (value.alertLabel == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.alertLabel);
        }
        final String _tmp;
        _tmp = __listTypeConverters.listIntToString(value.repeatDays);
        if (_tmp == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, _tmp);
        }
        stmt.bindLong(4, value.dateAlert);
        stmt.bindLong(5, value.alertTime);
        if (value.attachedPhoto == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.attachedPhoto);
        }
        final int _tmp_1;
        _tmp_1 = value.isKeepAlert ? 1 : 0;
        stmt.bindLong(7, _tmp_1);
      }
    };
    this.__deletionAdapterOfAlarmModel = new EntityDeletionOrUpdateAdapter<AlarmModel>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `AlarmModel` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, AlarmModel value) {
        stmt.bindLong(1, value.id);
      }
    };
    this.__updateAdapterOfAlarmModel = new EntityDeletionOrUpdateAdapter<AlarmModel>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `AlarmModel` SET `id` = ?,`alertLabel` = ?,`repeatDays` = ?,`dateAlert` = ?,`alertTime` = ?,`attachedPhoto` = ?,`isKeepAlert` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, AlarmModel value) {
        stmt.bindLong(1, value.id);
        if (value.alertLabel == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.alertLabel);
        }
        final String _tmp;
        _tmp = __listTypeConverters.listIntToString(value.repeatDays);
        if (_tmp == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, _tmp);
        }
        stmt.bindLong(4, value.dateAlert);
        stmt.bindLong(5, value.alertTime);
        if (value.attachedPhoto == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.attachedPhoto);
        }
        final int _tmp_1;
        _tmp_1 = value.isKeepAlert ? 1 : 0;
        stmt.bindLong(7, _tmp_1);
        stmt.bindLong(8, value.id);
      }
    };
  }

  @Override
  public void insertAlarm(AlarmModel alarmModel) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfAlarmModel.insert(alarmModel);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteAlarm(AlarmModel alarmModel) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfAlarmModel.handle(alarmModel);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateAlarm(AlarmModel alarmModel) {
    __db.beginTransaction();
    try {
      __updateAdapterOfAlarmModel.handle(alarmModel);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public AlarmModel[] queryAllAlarm() {
    final String _sql = "SELECT * FROM AlarmModel";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfAlertLabel = _cursor.getColumnIndexOrThrow("alertLabel");
      final int _cursorIndexOfRepeatDays = _cursor.getColumnIndexOrThrow("repeatDays");
      final int _cursorIndexOfDateAlert = _cursor.getColumnIndexOrThrow("dateAlert");
      final int _cursorIndexOfAlertTime = _cursor.getColumnIndexOrThrow("alertTime");
      final int _cursorIndexOfAttachedPhoto = _cursor.getColumnIndexOrThrow("attachedPhoto");
      final int _cursorIndexOfIsKeepAlert = _cursor.getColumnIndexOrThrow("isKeepAlert");
      final AlarmModel[] _result = new AlarmModel[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final AlarmModel _item;
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        final String _tmpAlertLabel;
        _tmpAlertLabel = _cursor.getString(_cursorIndexOfAlertLabel);
        final ArrayList<Integer> _tmpRepeatDays;
        final String _tmp;
        _tmp = _cursor.getString(_cursorIndexOfRepeatDays);
        _tmpRepeatDays = __listTypeConverters.stringToListInt(_tmp);
        final long _tmpDateAlert;
        _tmpDateAlert = _cursor.getLong(_cursorIndexOfDateAlert);
        final long _tmpAlertTime;
        _tmpAlertTime = _cursor.getLong(_cursorIndexOfAlertTime);
        final String _tmpAttachedPhoto;
        _tmpAttachedPhoto = _cursor.getString(_cursorIndexOfAttachedPhoto);
        final boolean _tmpIsKeepAlert;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfIsKeepAlert);
        _tmpIsKeepAlert = _tmp_1 != 0;
        _item = new AlarmModel(_tmpId,_tmpAlertLabel,_tmpRepeatDays,_tmpDateAlert,_tmpAlertTime,_tmpAttachedPhoto,_tmpIsKeepAlert);
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public AlarmModel queryAlarm(int id) {
    final String _sql = "SELECT * FROM AlarmModel WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfAlertLabel = _cursor.getColumnIndexOrThrow("alertLabel");
      final int _cursorIndexOfRepeatDays = _cursor.getColumnIndexOrThrow("repeatDays");
      final int _cursorIndexOfDateAlert = _cursor.getColumnIndexOrThrow("dateAlert");
      final int _cursorIndexOfAlertTime = _cursor.getColumnIndexOrThrow("alertTime");
      final int _cursorIndexOfAttachedPhoto = _cursor.getColumnIndexOrThrow("attachedPhoto");
      final int _cursorIndexOfIsKeepAlert = _cursor.getColumnIndexOrThrow("isKeepAlert");
      final AlarmModel _result;
      if(_cursor.moveToFirst()) {
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        final String _tmpAlertLabel;
        _tmpAlertLabel = _cursor.getString(_cursorIndexOfAlertLabel);
        final ArrayList<Integer> _tmpRepeatDays;
        final String _tmp;
        _tmp = _cursor.getString(_cursorIndexOfRepeatDays);
        _tmpRepeatDays = __listTypeConverters.stringToListInt(_tmp);
        final long _tmpDateAlert;
        _tmpDateAlert = _cursor.getLong(_cursorIndexOfDateAlert);
        final long _tmpAlertTime;
        _tmpAlertTime = _cursor.getLong(_cursorIndexOfAlertTime);
        final String _tmpAttachedPhoto;
        _tmpAttachedPhoto = _cursor.getString(_cursorIndexOfAttachedPhoto);
        final boolean _tmpIsKeepAlert;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfIsKeepAlert);
        _tmpIsKeepAlert = _tmp_1 != 0;
        _result = new AlarmModel(_tmpId,_tmpAlertLabel,_tmpRepeatDays,_tmpDateAlert,_tmpAlertTime,_tmpAttachedPhoto,_tmpIsKeepAlert);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
