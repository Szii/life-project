package com.delaroystudios.alarmreminder.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import com.delaroystudios.alarmreminder.alarm.AlarmDao;
import com.delaroystudios.alarmreminder.alarm.AlarmDao_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public class DBHelper_Impl extends DBHelper {
  private volatile AlarmDao _alarmDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `AlarmModel` (`id` INTEGER NOT NULL, `alertLabel` TEXT, `repeatDays` TEXT, `dateAlert` INTEGER NOT NULL, `alertTime` INTEGER NOT NULL, `attachedPhoto` TEXT, `isKeepAlert` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"b48c2b53931a41da6046f71ea133ee49\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `AlarmModel`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsAlarmModel = new HashMap<String, TableInfo.Column>(7);
        _columnsAlarmModel.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsAlarmModel.put("alertLabel", new TableInfo.Column("alertLabel", "TEXT", false, 0));
        _columnsAlarmModel.put("repeatDays", new TableInfo.Column("repeatDays", "TEXT", false, 0));
        _columnsAlarmModel.put("dateAlert", new TableInfo.Column("dateAlert", "INTEGER", true, 0));
        _columnsAlarmModel.put("alertTime", new TableInfo.Column("alertTime", "INTEGER", true, 0));
        _columnsAlarmModel.put("attachedPhoto", new TableInfo.Column("attachedPhoto", "TEXT", false, 0));
        _columnsAlarmModel.put("isKeepAlert", new TableInfo.Column("isKeepAlert", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysAlarmModel = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesAlarmModel = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoAlarmModel = new TableInfo("AlarmModel", _columnsAlarmModel, _foreignKeysAlarmModel, _indicesAlarmModel);
        final TableInfo _existingAlarmModel = TableInfo.read(_db, "AlarmModel");
        if (! _infoAlarmModel.equals(_existingAlarmModel)) {
          throw new IllegalStateException("Migration didn't properly handle AlarmModel(com.delaroystudios.alarmreminder.alarm.AlarmModel).\n"
                  + " Expected:\n" + _infoAlarmModel + "\n"
                  + " Found:\n" + _existingAlarmModel);
        }
      }
    }, "b48c2b53931a41da6046f71ea133ee49", "4b57db0237da18cc8190f6564dac0848");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "AlarmModel");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `AlarmModel`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public AlarmDao alarmDao() {
    if (_alarmDao != null) {
      return _alarmDao;
    } else {
      synchronized(this) {
        if(_alarmDao == null) {
          _alarmDao = new AlarmDao_Impl(this);
        }
        return _alarmDao;
      }
    }
  }
}
