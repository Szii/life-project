// Generated code from Butter Knife. Do not modify!
package com.delaroystudios.alarmreminder.alarm;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.delaroystudios.alarmreminder.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AlarmItemViewHolder_ViewBinding implements Unbinder {
  private AlarmItemViewHolder target;

  @UiThread
  public AlarmItemViewHolder_ViewBinding(AlarmItemViewHolder target, View source) {
    this.target = target;

    target.tvTime = Utils.findRequiredViewAsType(source, R.id.tv_time, "field 'tvTime'", TextView.class);
    target.tvSuffix = Utils.findRequiredViewAsType(source, R.id.tv_time_suffix, "field 'tvSuffix'", TextView.class);
    target.imgRepeat = Utils.findRequiredViewAsType(source, R.id.img_repeat, "field 'imgRepeat'", ImageView.class);
    target.tvRepeatDays = Utils.findRequiredViewAsType(source, R.id.tv_repeat_week_day, "field 'tvRepeatDays'", TextView.class);
    target.imgAlarmStatus = Utils.findRequiredViewAsType(source, R.id.img_alarm_status, "field 'imgAlarmStatus'", ImageView.class);
    target.rootItem = Utils.findRequiredViewAsType(source, R.id.root_item, "field 'rootItem'", RelativeLayout.class);
    target.relativeAlarm = Utils.findRequiredViewAsType(source, R.id.rela_alarm, "field 'relativeAlarm'", RelativeLayout.class);
    target.tvAlertName = Utils.findRequiredViewAsType(source, R.id.tv_alert_name, "field 'tvAlertName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AlarmItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvTime = null;
    target.tvSuffix = null;
    target.imgRepeat = null;
    target.tvRepeatDays = null;
    target.imgAlarmStatus = null;
    target.rootItem = null;
    target.relativeAlarm = null;
    target.tvAlertName = null;
  }
}
