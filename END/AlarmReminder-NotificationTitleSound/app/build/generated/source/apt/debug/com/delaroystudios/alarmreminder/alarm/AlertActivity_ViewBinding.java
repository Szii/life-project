// Generated code from Butter Knife. Do not modify!
package com.delaroystudios.alarmreminder.alarm;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.delaroystudios.alarmreminder.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AlertActivity_ViewBinding implements Unbinder {
  private AlertActivity target;

  @UiThread
  public AlertActivity_ViewBinding(AlertActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AlertActivity_ViewBinding(AlertActivity target, View source) {
    this.target = target;

    target.imgAlarm = Utils.findRequiredViewAsType(source, R.id.img_alarm, "field 'imgAlarm'", ImageView.class);
    target.tvTime = Utils.findRequiredViewAsType(source, R.id.tv_time, "field 'tvTime'", TextView.class);
    target.tvSuffix = Utils.findRequiredViewAsType(source, R.id.tv_time_suffix, "field 'tvSuffix'", TextView.class);
    target.tvAlertName = Utils.findRequiredViewAsType(source, R.id.tv_alert_name, "field 'tvAlertName'", TextView.class);
    target.btnDismiss = Utils.findRequiredViewAsType(source, R.id.bt_dismiss, "field 'btnDismiss'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AlertActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgAlarm = null;
    target.tvTime = null;
    target.tvSuffix = null;
    target.tvAlertName = null;
    target.btnDismiss = null;
  }
}
