// Generated code from Butter Knife. Do not modify!
package com.delaroystudios.alarmreminder.alarm;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.delaroystudios.alarmreminder.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AlarmActivity_ViewBinding implements Unbinder {
  private AlarmActivity target;

  @UiThread
  public AlarmActivity_ViewBinding(AlarmActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AlarmActivity_ViewBinding(AlarmActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.recyclerListAlarm = Utils.findRequiredViewAsType(source, R.id.recycler_list_alarm, "field 'recyclerListAlarm'", RecyclerView.class);
    target.fab = Utils.findRequiredViewAsType(source, R.id.fab_add_alarm, "field 'fab'", FloatingActionButton.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progress_bar, "field 'progressBar'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AlarmActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.recyclerListAlarm = null;
    target.fab = null;
    target.progressBar = null;
  }
}
