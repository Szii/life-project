// Generated code from Butter Knife. Do not modify!
package com.delaroystudios.alarmreminder.alarm;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.delaroystudios.alarmreminder.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateAlarmActivity_ViewBinding implements Unbinder {
  private CreateAlarmActivity target;

  @UiThread
  public CreateAlarmActivity_ViewBinding(CreateAlarmActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CreateAlarmActivity_ViewBinding(CreateAlarmActivity target, View source) {
    this.target = target;

    target.btnCancel = Utils.findRequiredViewAsType(source, R.id.btn_cancel, "field 'btnCancel'", Button.class);
    target.btnSave = Utils.findRequiredViewAsType(source, R.id.btn_save, "field 'btnSave'", Button.class);
    target.relativeDate = Utils.findRequiredViewAsType(source, R.id.rela_date, "field 'relativeDate'", RelativeLayout.class);
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.tv_date, "field 'tvDate'", TextView.class);
    target.timePicker = Utils.findRequiredViewAsType(source, R.id.time_picker, "field 'timePicker'", TimePicker.class);
    target.etAlarmName = Utils.findRequiredViewAsType(source, R.id.et_alarm_name, "field 'etAlarmName'", EditText.class);
    target.btAddImage = Utils.findRequiredViewAsType(source, R.id.bt_add_image, "field 'btAddImage'", ImageButton.class);
    target.imgAlarm = Utils.findRequiredViewAsType(source, R.id.img_alarm, "field 'imgAlarm'", ImageView.class);
    target.tvSunday = Utils.findRequiredViewAsType(source, R.id.tv_sunday, "field 'tvSunday'", TextView.class);
    target.tvMonday = Utils.findRequiredViewAsType(source, R.id.tv_monday, "field 'tvMonday'", TextView.class);
    target.tvTuesday = Utils.findRequiredViewAsType(source, R.id.tv_tuesday, "field 'tvTuesday'", TextView.class);
    target.tvWednesday = Utils.findRequiredViewAsType(source, R.id.tv_wednesday, "field 'tvWednesday'", TextView.class);
    target.tvThursday = Utils.findRequiredViewAsType(source, R.id.tv_thursday, "field 'tvThursday'", TextView.class);
    target.tvFriday = Utils.findRequiredViewAsType(source, R.id.tv_friday, "field 'tvFriday'", TextView.class);
    target.tvSaturday = Utils.findRequiredViewAsType(source, R.id.tv_saturday, "field 'tvSaturday'", TextView.class);
    target.rootView = Utils.findRequiredViewAsType(source, R.id.rootView, "field 'rootView'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateAlarmActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnCancel = null;
    target.btnSave = null;
    target.relativeDate = null;
    target.tvDate = null;
    target.timePicker = null;
    target.etAlarmName = null;
    target.btAddImage = null;
    target.imgAlarm = null;
    target.tvSunday = null;
    target.tvMonday = null;
    target.tvTuesday = null;
    target.tvWednesday = null;
    target.tvThursday = null;
    target.tvFriday = null;
    target.tvSaturday = null;
    target.rootView = null;
  }
}
