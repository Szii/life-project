package com.delaroystudios.alarmreminder.database;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Me on 12/10/2018 AD.
 */
public class ListTypeConverters {
    Gson gson = new Gson();

    @TypeConverter
    public ArrayList<Integer> stringToListInt(String json) {
        if (json.equals("null")) {
            return new ArrayList<>();
        }

        Type listType = new TypeToken<ArrayList<Integer>>() {
        }.getType();
        return gson.fromJson(json, listType);
    }

    @TypeConverter
    public String listIntToString(ArrayList<Integer> list) {
        if (list.size() == 0)
            return "null";
        else
            return gson.toJson(list);
    }
}
