package com.delaroystudios.alarmreminder.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.delaroystudios.alarmreminder.Constant;

/**
 * Created by Me on 12/9/2018 AD.
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, AlertActivity.class);
        int id = intent.getIntExtra(Constant.ALARM_ID, 0);
        i.putExtra(Constant.ALARM_ID, id);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
