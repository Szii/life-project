package com.delaroystudios.alarmreminder.database;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.support.annotation.NonNull;

import com.delaroystudios.alarmreminder.Constant;
import com.delaroystudios.alarmreminder.alarm.AlarmDao;
import com.delaroystudios.alarmreminder.alarm.AlarmModel;

/**
 * Created by Me on 12/10/2018 AD.
 */
@Database(entities = {AlarmModel.class}, version = Constant.DB_VERSION)
@TypeConverters(ListTypeConverters.class)
public abstract class DBHelper extends RoomDatabase {
    public abstract AlarmDao alarmDao();

    public static DBHelper getInstance(Context context) {
        DBHelper dbHelper = Room.databaseBuilder(context,
                DBHelper.class, Constant.DB_NAME).fallbackToDestructiveMigration().build();
        return dbHelper;
    }

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {

    }
}
