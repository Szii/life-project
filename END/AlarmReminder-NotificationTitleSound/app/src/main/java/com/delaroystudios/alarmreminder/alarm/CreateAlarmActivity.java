package com.delaroystudios.alarmreminder.alarm;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.delaroystudios.alarmreminder.Constant;
import com.delaroystudios.alarmreminder.R;
import com.delaroystudios.alarmreminder.database.DBHelper;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateAlarmActivity extends AppCompatActivity {
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.rela_date)
    RelativeLayout relativeDate;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.time_picker)
    TimePicker timePicker;
    @BindView(R.id.et_alarm_name)
    EditText etAlarmName;
    @BindView(R.id.bt_add_image)
    ImageButton btAddImage;
    @BindView(R.id.img_alarm)
    ImageView imgAlarm;
    @BindView(R.id.tv_sunday)
    TextView tvSunday;
    @BindView(R.id.tv_monday)
    TextView tvMonday;
    @BindView(R.id.tv_tuesday)
    TextView tvTuesday;
    @BindView(R.id.tv_wednesday)
    TextView tvWednesday;
    @BindView(R.id.tv_thursday)
    TextView tvThursday;
    @BindView(R.id.tv_friday)
    TextView tvFriday;
    @BindView(R.id.tv_saturday)
    TextView tvSaturday;
    @BindView(R.id.rootView)
    LinearLayout rootView;

    private DatePickerDialog datePickerDialog;
    private int currentHours, currentMins, _id;
    private Date chosenDate = new Date(), currentTime = new Date();
    private String encodedImage;
    private Calendar alertTime = Calendar.getInstance(), calendar = Calendar.getInstance();
    private ArrayList<Integer> repeatSelected = new ArrayList<>();
    private boolean isSun = false, isMon = false, isTue = false, isWed = false, isThurs = false, isFri = false, isSat = false;
    private AlarmModel alarmModel;
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_alarm);
        ButterKnife.bind(this);
        if (getIntent().getParcelableExtra(Constant.ALARM_MODEL) != null) {
            alarmModel = getIntent().getParcelableExtra(Constant.ALARM_MODEL);
            isEdit = true;
            _id = alarmModel.id;
            calendar.setTimeInMillis(alarmModel.getDateAlert());
            chosenDate = new Date(alarmModel.dateAlert);
            if (checkCurrentDate())
                currentTime = new Date();
            else
                currentTime.setTime(alarmModel.getAlertTime());
            Calendar cl = Calendar.getInstance();
            cl.setTimeInMillis(alarmModel.alertTime);
            alertTime = cl;
            repeatSelected.addAll(alarmModel.repeatDays);
            etAlarmName.setText(alarmModel.alertLabel);
            encodedImage = alarmModel.attachedPhoto;
        }
        initStatusBar();
        initDatePickerDialog();
        initInstance();
        initRepeatDay();
        initPhoto();
    }


    private void initStatusBar() {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.toolbar));
    }

    @SuppressLint("SetTextI18n")
    private void initDatePickerDialog() {
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        changeDateFormat(year, month, dayOfMonth);

        datePickerDialog = new DatePickerDialog(this,
                (datePicker, yearPicker, monthPicker, dayPicker) -> {
                    monthPicker += 1;
                    changeDateFormat(yearPicker, monthPicker, dayPicker);
                }, year, month - 1, dayOfMonth);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    }

    private void changeDateFormat(int year, int month, int dayOfMonth) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat output = new SimpleDateFormat("E, MMM dd");
        String inputDateStr = year + "-" + month + "-" + dayOfMonth;
        try {
            chosenDate = input.parse(inputDateStr);

            String outputDateStr = output.format(chosenDate);
            tvDate.setText(outputDateStr);
            currentTime = new Date();

            Calendar calendarChosen = Calendar.getInstance();
            calendarChosen.setTime(chosenDate);
            alertTime.set(Calendar.YEAR, calendarChosen.get(Calendar.YEAR));
            alertTime.set(Calendar.MONTH, calendarChosen.get(Calendar.MONTH));
            alertTime.set(Calendar.DATE, calendarChosen.get(Calendar.DATE));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initInstance() {
        rootView.setOnTouchListener((view, motionEvent) -> {
            hideSoftKeyboard();
            return false;
        });
        btnCancel.setOnClickListener(v -> onBackPressed());
        btnSave.setOnClickListener(v -> {
            AlarmModel savedAlarmModel = new AlarmModel(_id, etAlarmName.getText().toString(), repeatSelected, chosenDate.getTime(), alertTime.getTimeInMillis(), encodedImage, true);
            if (isEdit) {
                Constant.cancelAlarm(this, savedAlarmModel);
                updateData(savedAlarmModel);
            } else {
                _id = (int) System.currentTimeMillis();
                savedAlarmModel.id = _id;
                insertData(savedAlarmModel);
            }
            createAlarm(savedAlarmModel);
        });

        relativeDate.setOnClickListener(v -> datePickerDialog.show());
        btAddImage.setOnClickListener(v -> {
            ImagePicker.create(this)
                    .returnMode(ReturnMode.ALL)
                    .folderMode(true)
                    .includeVideo(false)
                    .single()
                    .limit(1)
                    .imageDirectory("Camera")
                    .start();
        });

        timePicker.setIs24HourView(false);
        initTimePicker();

        timePicker.setOnTimeChangedListener((timePicker, hourOfDay, minute) -> {
            if (checkCurrentDate()) {
                if (hourOfDay < currentHours || (hourOfDay == currentHours && minute < currentMins)) {
                    Toast.makeText(this, "Cannot set alarm for times in the past.", Toast.LENGTH_SHORT).show();
                    initTimePicker();
                } else {
                    alertTime = Calendar.getInstance();
                    alertTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    alertTime.set(Calendar.MINUTE, minute);
                    alertTime.set(Calendar.SECOND, 0);
                    alertTime.set(Calendar.MILLISECOND, 0);
                }
            } else {
                alertTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                alertTime.set(Calendar.MINUTE, minute);
                alertTime.set(Calendar.SECOND, 0);
                alertTime.set(Calendar.MILLISECOND, 0);
            }
        });


    }

    @SuppressLint("StaticFieldLeak")
    private void createAlarm(AlarmModel savedAlarmModel) {
        if (repeatSelected.size() == 0) { /* It means that you have specified day to alert*/
            Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
            intent.putExtra(Constant.ALARM_ID, _id);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), _id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alertTime.getTimeInMillis(), pendingIntent);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, alertTime.getTimeInMillis(), pendingIntent);
                }
            }
            Intent intentToList = new Intent();
            intentToList.putExtra(Constant.ALARM_MODEL, savedAlarmModel);
            setResult(RESULT_OK, intentToList);
            finish();
        } else {
            if (repeatSelected.size() != 7) {
                for (int i = 0; i < repeatSelected.size(); i++) {
                    if (repeatSelected.get(i) == Calendar.SUNDAY)
                        forDay(Calendar.SUNDAY, false);
                    if (repeatSelected.get(i) == Calendar.MONDAY)
                        forDay(Calendar.MONDAY, false);
                    if (repeatSelected.get(i) == Calendar.TUESDAY)
                        forDay(Calendar.TUESDAY, false);
                    if (repeatSelected.get(i) == Calendar.WEDNESDAY)
                        forDay(Calendar.WEDNESDAY, false);
                    if (repeatSelected.get(i) == Calendar.THURSDAY)
                        forDay(Calendar.THURSDAY, false);
                    if (repeatSelected.get(i) == Calendar.FRIDAY)
                        forDay(Calendar.FRIDAY, false);
                    if (repeatSelected.get(i) == Calendar.SATURDAY)
                        forDay(Calendar.SATURDAY, false);
                }
            } else {
                forDay(alertTime.get(Calendar.DAY_OF_WEEK), true);
            }

            Intent intentToList = new Intent();
            intentToList.putExtra(Constant.ALARM_MODEL, savedAlarmModel);
            setResult(RESULT_OK, intentToList);
            finish();
        }
    }

    public void forDay(int day, boolean isWeekly) {
        Calendar alert = Calendar.getInstance();
        alert.set(Calendar.DAY_OF_WEEK, day);
        alert.set(Calendar.HOUR_OF_DAY, alertTime.get(Calendar.HOUR_OF_DAY));
        alert.set(Calendar.MINUTE, alertTime.get(Calendar.MINUTE));
        alert.set(Calendar.SECOND, 0);
        alert.set(Calendar.MILLISECOND, 0);
        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        intent.putExtra(Constant.ALARM_ID, _id);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), _id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            if (isWeekly)
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alert.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pendingIntent);
            else {
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (alert.get(Calendar.DAY_OF_WEEK) == today.get(Calendar.DAY_OF_WEEK)) {
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        // Do something after 5ms = 500ms
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alert.getTimeInMillis(), pendingIntent);
                        }
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alert.getTimeInMillis(), 24 * 60 * 60 * 1000, pendingIntent);
                    }, 500);
                } else {
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alert.getTimeInMillis(), 24 * 60 * 60 * 1000, pendingIntent);
                }
            }
        }
    }

    private void updateData(AlarmModel savedAlarmModel) {
        new Thread(() -> {
            DBHelper.getInstance(CreateAlarmActivity.this).alarmDao().updateAlarm(savedAlarmModel);
        }).start();
    }

    private void insertData(AlarmModel alarmModel) {
        new Thread(() -> {
            DBHelper.getInstance(CreateAlarmActivity.this).alarmDao().insertAlarm(alarmModel);
        }).start();
    }

    private void initRepeatDay() {
        initClickRepeat();
        if (repeatSelected.size() != 0) {
            for (int i = 0; i < repeatSelected.size(); i++) {
                switch (repeatSelected.get(i)) {
                    case Calendar.SUNDAY:
                        isSun = true;
                        break;
                    case Calendar.MONDAY:
                        isMon = true;
                        break;
                    case Calendar.TUESDAY:
                        isTue = true;
                        break;
                    case Calendar.WEDNESDAY:
                        isWed = true;
                        break;
                    case Calendar.THURSDAY:
                        isThurs = true;
                        break;
                    case Calendar.FRIDAY:
                        isFri = true;
                        break;
                    case Calendar.SATURDAY:
                        isSat = true;
                        break;
                }
            }
        }
        changeTextColor();
    }

    private void initClickRepeat() {
        tvSunday.setOnClickListener(view -> {
            if (isSun) {
                repeatSelected.remove(Integer.valueOf(Calendar.SUNDAY));
                isSun = false;
            } else {
                repeatSelected.add(Calendar.SUNDAY);
                isSun = true;
            }
            tvSunday.setSelected(isSun);
        });
        tvMonday.setOnClickListener(view -> {
            if (isMon) {
                repeatSelected.remove(Integer.valueOf(Calendar.MONDAY));
                isMon = false;
            } else {
                repeatSelected.add(Calendar.MONDAY);
                isMon = true;
            }
            tvMonday.setSelected(isMon);
        });
        tvTuesday.setOnClickListener(view -> {
            if (isTue) {
                repeatSelected.remove(Integer.valueOf(Calendar.TUESDAY));
                isTue = false;
            } else {
                repeatSelected.add(Calendar.TUESDAY);
                isTue = true;
            }
            tvTuesday.setSelected(isTue);
        });
        tvWednesday.setOnClickListener(view -> {
            if (isWed) {
                repeatSelected.remove(Integer.valueOf(Calendar.WEDNESDAY));
                isWed = false;
            } else {
                repeatSelected.add(Calendar.WEDNESDAY);
                isWed = true;
            }
            tvWednesday.setSelected(isWed);
        });
        tvThursday.setOnClickListener(view -> {
            if (isThurs) {
                repeatSelected.remove(Integer.valueOf(Calendar.THURSDAY));
                isThurs = false;
            } else {
                repeatSelected.add(Calendar.THURSDAY);
                isThurs = true;
            }
            tvThursday.setSelected(isThurs);
        });
        tvFriday.setOnClickListener(view -> {
            if (isFri) {
                repeatSelected.remove(Integer.valueOf(Calendar.FRIDAY));
                isFri = false;
            } else {
                repeatSelected.add(Calendar.FRIDAY);
                isFri = true;
            }
            tvFriday.setSelected(isFri);
        });
        tvSaturday.setOnClickListener(view -> {
            if (isSat) {
                repeatSelected.remove(Integer.valueOf(Calendar.SATURDAY));
                isSat = false;
            } else {
                repeatSelected.add(Calendar.SATURDAY);
                isSat = true;
            }
            tvSaturday.setSelected(isSat);
        });
    }

    private void initPhoto() {
        if (encodedImage != null && !encodedImage.equals(""))
            Glide.with(this).load(Constant.convertBase64StringToByteArray(encodedImage)).into(imgAlarm);
    }

    private void changeTextColor() {
        tvSunday.setSelected(isSun);
        tvMonday.setSelected(isMon);
        tvTuesday.setSelected(isTue);
        tvWednesday.setSelected(isWed);
        tvThursday.setSelected(isThurs);
        tvFriday.setSelected(isFri);
        tvSaturday.setSelected(isSat);
    }

    private boolean checkCurrentDate() {
        return DateUtils.isToday(chosenDate.getTime());
    }

    private void initTimePicker() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentTime);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        currentMins = cal.get(Calendar.MINUTE);
        currentHours = cal.get(Calendar.HOUR_OF_DAY);
        if (currentMins == 59) {
            currentHours += 1;
            currentMins = 0;
        } else {
            currentMins += 1;
        }
        alertTime.set(Calendar.HOUR_OF_DAY, currentHours);
        alertTime.set(Calendar.MINUTE, currentMins);
        alertTime.set(Calendar.SECOND, 0);
        alertTime.set(Calendar.MILLISECOND, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker.setHour(currentHours);
            timePicker.setMinute(currentMins);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // get a single image only
            Image image = ImagePicker.getFirstImageOrNull(data);
            Glide.with(this).load(image.getPath()).into(imgAlarm);

            //Make it easier to save into database
            encodedImage = Constant.convertImageToBase64String(image.getPath());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Make touch outside edittext to hide softkeyboard
    private void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
