package com.delaroystudios.alarmreminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.delaroystudios.alarmreminder.alarm.AlarmModel;
import com.delaroystudios.alarmreminder.alarm.AlarmReceiver;

import java.io.ByteArrayOutputStream;

public class Constant {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "life_db";

    public static final String ALARM_MODEL = "alarmModel";
    public static final String ALARM_ID = "alarmId";

    public static String convertImageToBase64String(String path) {
        Bitmap bm = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.WEBP, 70, baos); //bm is the bitmap object
        byte[] byteArrayImage = baos.toByteArray();
        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    public static byte[] convertBase64StringToByteArray(String base64) {
        return Base64.decode(base64, Base64.DEFAULT);
    }
    public static void cancelAlarm(Context mContext, AlarmModel alarmModel) {
        Intent mIntent = new Intent(mContext, AlarmReceiver.class);
        mIntent.putExtra(Constant.ALARM_ID, alarmModel.id);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext,
                alarmModel.id, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        pendingIntent.cancel();
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
    }
}
