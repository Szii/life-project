package com.delaroystudios.alarmreminder.alarm;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.delaroystudios.alarmreminder.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Me on 12/9/2018 AD.
 */
public class AlarmItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_time_suffix)
    TextView tvSuffix;
    @BindView(R.id.img_repeat)
    ImageView imgRepeat;
    @BindView(R.id.tv_repeat_week_day)
    TextView tvRepeatDays;
    @BindView(R.id.img_alarm_status)
    ImageView imgAlarmStatus;
    @BindView(R.id.root_item)
    RelativeLayout rootItem;
    @BindView(R.id.rela_alarm)
    RelativeLayout relativeAlarm;
    @BindView(R.id.tv_alert_name)
    TextView tvAlertName;
    private AlarmListAdapter.OnAlarmItemListener listener;
    private String suffix = "AM", hour = "", min = "";

    public AlarmItemViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindView(AlarmListAdapter.OnAlarmItemListener listener, AlarmModel alarmModel) {
        this.listener = listener;
        rootItem.setOnClickListener(view -> listener.onItemClick(alarmModel));
        rootItem.setOnLongClickListener(view -> {
            listener.onItemLongClick(getAdapterPosition());
            return true;
        });
        imgAlarmStatus.setSelected(alarmModel.isKeepAlert);
        relativeAlarm.setOnClickListener(view -> {
            if (alarmModel.isKeepAlert)
                imgAlarmStatus.setSelected(false);
            else
                imgAlarmStatus.setSelected(true);
            alarmModel.isKeepAlert = imgAlarmStatus.isSelected();
            listener.onAlarmClick(alarmModel);
        });

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(alarmModel.alertTime));
        int hours = cal.get(Calendar.HOUR_OF_DAY);
        int mins = cal.get(Calendar.MINUTE);
        if (hours < 10)
            hour = 0 + "" + hours;
        else
            hour = String.valueOf(hours);
        if (mins < 10)
            min = 0 + "" + mins;
        else
            min = String.valueOf(mins);
        if (cal.get(Calendar.AM_PM) == 1)
            suffix = "PM";
        else
            suffix = "AM";

        tvTime.setText(hour + ":" + min);
        tvSuffix.setText(suffix);

        SimpleDateFormat output = new SimpleDateFormat("E, MMM dd");
        if (alarmModel.repeatDays.size() == 0) {
            imgRepeat.setVisibility(View.GONE);
            tvRepeatDays.setText(output.format(new Date(alarmModel.dateAlert)));
        } else {
            imgRepeat.setVisibility(View.VISIBLE);
            StringBuilder repeat = new StringBuilder();
            Collections.sort(alarmModel.repeatDays);
            for (int day : alarmModel.repeatDays) {
                switch (day) {
                    case Calendar.SUNDAY:
                        repeat.append("Sun ");
                        break;
                    case Calendar.MONDAY:
                        repeat.append("Mon ");
                        break;
                    case Calendar.TUESDAY:
                        repeat.append("Tue ");
                        break;
                    case Calendar.WEDNESDAY:
                        repeat.append("Wed ");
                        break;
                    case Calendar.THURSDAY:
                        repeat.append("Thurs ");
                        break;
                    case Calendar.FRIDAY:
                        repeat.append("Fri ");
                        break;
                    case Calendar.SATURDAY:
                        repeat.append("Sat ");
                        break;
                }
            }
            tvRepeatDays.setText(repeat.toString());
        }

        if (alarmModel.alertLabel.equals("")) {
            tvAlertName.setVisibility(View.GONE);
        } else {
            tvAlertName.setVisibility(View.VISIBLE);
            tvAlertName.setText(alarmModel.alertLabel);
        }
    }

}
