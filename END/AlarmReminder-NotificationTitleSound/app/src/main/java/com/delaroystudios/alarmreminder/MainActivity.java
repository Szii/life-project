package com.delaroystudios.alarmreminder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.delaroystudios.alarmreminder.alarm.AlarmActivity;

public class MainActivity extends Activity implements View.OnClickListener {

    Button pro, alarm, calendar, sos, manual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pro = findViewById(R.id.btpro);
        pro.setOnClickListener(this);
        sos = findViewById(R.id.btsos);
        sos.setOnClickListener(this);
        calendar = findViewById(R.id.btschuedule);
        calendar.setOnClickListener(this);
        alarm = findViewById(R.id.btalarm);
        alarm.setOnClickListener(this);
        manual = findViewById(R.id.btmanual);
        manual.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.btpro):
                Intent itn = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(itn);
                break;
            case (R.id.btsos):
                Intent itn2 = new Intent(MainActivity.this, SosActivity.class);
                startActivity(itn2);
                break;
            case (R.id.btschuedule):
                Intent itn3 = new Intent(MainActivity.this, calendar.class);
                startActivity(itn3);
                break;
            case (R.id.btalarm):
                Intent itn4 = new Intent(MainActivity.this, AlarmActivity.class);
                startActivity(itn4);
                break;
            case (R.id.btmanual):
                Intent itn5 = new Intent(MainActivity.this, ManualActivity.class);
                startActivity(itn5);
                break;
        }
    }
}
