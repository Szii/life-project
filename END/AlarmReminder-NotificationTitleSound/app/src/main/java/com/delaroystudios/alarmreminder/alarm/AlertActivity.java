package com.delaroystudios.alarmreminder.alarm;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.delaroystudios.alarmreminder.Constant;
import com.delaroystudios.alarmreminder.R;
import com.delaroystudios.alarmreminder.database.DBHelper;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlertActivity extends AppCompatActivity {
    @BindView(R.id.img_alarm)
    ImageView imgAlarm;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_time_suffix)
    TextView tvSuffix;
    @BindView(R.id.tv_alert_name)
    TextView tvAlertName;
    @BindView(R.id.bt_dismiss)
    Button btnDismiss;

    PowerManager powerManager;
    PowerManager.WakeLock wakeLock;
    KeyguardManager keyguardManager;
    KeyguardManager.KeyguardLock keyguardLock;
    Ringtone ringtone;

    private Context mContext;
    private AlarmModel mAlarmModel = new AlarmModel();
    private boolean isDismiss = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        keyguardLock = keyguardManager.newKeyguardLock("ShowEvent");
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "ShowEvent");
        wakeLock.acquire(3 * 60 * 1000L /*3 minutes*/); //wake up the screen
        keyguardLock.disableKeyguard();

        setContentView(R.layout.activity_alert);
        ButterKnife.bind(this);
        mContext = this;
        initInstance();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
    }

    private void initInstance() {
        if (getIntent().getIntExtra(Constant.ALARM_ID, 0) != 0) {
            int id = getIntent().getIntExtra(Constant.ALARM_ID, 0);
            queryAlarmModel(id);
        }

        btnDismiss.setOnClickListener(v -> {
            isDismiss = true;
            if (mAlarmModel.repeatDays.size() == 0) {
                mAlarmModel.isKeepAlert = false;
                updateRepeatStatus(mAlarmModel);
            } else {
                finishAndRemoveTask();
            }
        });

    }

    @SuppressLint("StaticFieldLeak")
    private void updateRepeatStatus(AlarmModel mAlarmModel) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                DBHelper.getInstance(mContext).alarmDao().updateAlarm(mAlarmModel);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (isDismiss) {
                    finishAndRemoveTask();
                }
//                else {
//                    Intent intent = new Intent(mContext, HomeActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                    finish();
//                }
            }
        }.execute();
    }

    @Override
    public void onBackPressed() {
    }

    @SuppressLint("StaticFieldLeak")
    private void queryAlarmModel(int id) {
        new AsyncTask<Void, Void, AlarmModel>() {
            @Override
            protected AlarmModel doInBackground(Void... voids) {
                AlarmModel alarmModel = DBHelper.getInstance(mContext).alarmDao().queryAlarm(id);
                if (alarmModel != null)
                    return alarmModel;
                else {
                    alarmModel = new AlarmModel();
                    alarmModel.id = id;
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    alarmModel.alertTime = calendar.getTimeInMillis();
                    return alarmModel;
                }
            }

            @Override
            protected void onPostExecute(AlarmModel alarmModel) {
                super.onPostExecute(alarmModel);
                mAlarmModel = alarmModel;
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date(alarmModel.alertTime));
                int hours = cal.get(Calendar.HOUR_OF_DAY);
                int mins = cal.get(Calendar.MINUTE);
                String hour = "00", min = "00", suffix = "AM";
                if (hours < 10)
                    hour = 0 + "" + hours;
                else
                    hour = String.valueOf(hours);
                if (mins < 10)
                    min = 0 + "" + mins;
                else
                    min = String.valueOf(mins);
                if (cal.get(Calendar.AM_PM) == 1)
                    suffix = "PM";
                else
                    suffix = "AM";

                tvTime.setText(hour + ":" + min);
                tvSuffix.setText(suffix);
                if (alarmModel.alertLabel.equals("")) {
                    tvAlertName.setVisibility(View.GONE);
                } else {
                    tvAlertName.setVisibility(View.VISIBLE);
                    tvAlertName.setText(alarmModel.alertLabel);
                }

                if (alarmModel.attachedPhoto != null && !alarmModel.attachedPhoto.equals("")) {
                    Glide.with(mContext).load(Constant.convertBase64StringToByteArray(alarmModel.attachedPhoto)).into(imgAlarm);
                }
            }
        }.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        wakeLock.acquire(3 * 60 * 1000L /*3 minutes*/);//must call this!
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        if (notification == null) {
            notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            if (notification == null) {
                notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            }
        }
        ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
        ringtone.play();
    }

    @Override
    protected void onPause() {
        super.onPause();
        wakeLock.release();
        if (ringtone.isPlaying()) {
            ringtone.stop();
        }
    }
}
