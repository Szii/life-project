package com.delaroystudios.alarmreminder.alarm;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

/**
 * Created by Me on 12/10/2018 AD.
 */
@Dao
public interface AlarmDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAlarm(AlarmModel alarmModel);

    @Delete
    void deleteAlarm(AlarmModel alarmModel);

    @Update
    void updateAlarm(AlarmModel alarmModel);

    @Query("SELECT * FROM AlarmModel")
    AlarmModel[] queryAllAlarm();

    @Query("SELECT * FROM AlarmModel WHERE id = :id")
    AlarmModel queryAlarm(int id);
}
