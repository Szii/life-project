package com.delaroystudios.alarmreminder.alarm;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Me on 12/9/2018 AD.
 */
@Entity
public class AlarmModel implements Parcelable {
    @PrimaryKey
    public int id = 0;
    public String alertLabel = "";
    public ArrayList<Integer> repeatDays = new ArrayList<>();
    public long dateAlert = 0;
    public long alertTime = 0;
    public String attachedPhoto = "";
    public boolean isKeepAlert = true;

    @Ignore
    public AlarmModel() {
    }

    public AlarmModel(int id, String alertLabel, ArrayList<Integer> repeatDays, long dateAlert, long alertTime, String attachedPhoto, boolean isKeepAlert) {
        this.id = id;
        this.alertLabel = alertLabel;
        this.repeatDays = repeatDays;
        this.dateAlert = dateAlert;
        this.alertTime = alertTime;
        this.attachedPhoto = attachedPhoto;
        this.isKeepAlert = isKeepAlert;
    }

    protected AlarmModel(Parcel in) {
        id = in.readInt();
        alertLabel = in.readString();
        dateAlert = in.readLong();
        alertTime = in.readLong();
        attachedPhoto = in.readString();
        isKeepAlert = in.readByte() != 0;
        repeatDays = in.readArrayList(null);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(alertLabel);
        dest.writeLong(dateAlert);
        dest.writeLong(alertTime);
        dest.writeString(attachedPhoto);
        dest.writeByte((byte) (isKeepAlert ? 1 : 0));
        dest.writeList(repeatDays);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AlarmModel> CREATOR = new Creator<AlarmModel>() {
        @Override
        public AlarmModel createFromParcel(Parcel in) {
            return new AlarmModel(in);
        }

        @Override
        public AlarmModel[] newArray(int size) {
            return new AlarmModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAlertLabel() {
        return alertLabel;
    }

    public void setAlertLabel(String alertLabel) {
        this.alertLabel = alertLabel;
    }

    public ArrayList<Integer> getRepeatDays() {
        return repeatDays;
    }

    public void setRepeatDays(ArrayList<Integer> repeatDays) {
        this.repeatDays = repeatDays;
    }

    public long getDateAlert() {
        return dateAlert;
    }

    public void setDateAlert(long dateAlert) {
        this.dateAlert = dateAlert;
    }

    public long getAlertTime() {
        return alertTime;
    }

    public void setAlertTime(long alertTime) {
        this.alertTime = alertTime;
    }

    public String getAttachedPhoto() {
        return attachedPhoto;
    }

    public void setAttachedPhoto(String attachedPhoto) {
        this.attachedPhoto = attachedPhoto;
    }

    public boolean isKeepAlert() {
        return isKeepAlert;
    }

    public void setKeepAlert(boolean keepAlert) {
        isKeepAlert = keepAlert;
    }

    public static Creator<AlarmModel> getCREATOR() {
        return CREATOR;
    }
}
