package com.delaroystudios.alarmreminder.alarm;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.delaroystudios.alarmreminder.Constant;
import com.delaroystudios.alarmreminder.R;
import com.delaroystudios.alarmreminder.database.DBHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlarmActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_list_alarm)
    RecyclerView recyclerListAlarm;
    @BindView(R.id.fab_add_alarm)
    FloatingActionButton fab;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private AlarmListAdapter alarmListAdapter;
    private int REQUEST_CODE_ADD = 101;
    private int REQUEST_CODE_EDIT = 102;
    private ArrayList<AlarmModel> alarmModels = new ArrayList<>();
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        ButterKnife.bind(this);
        mContext = this;
        initToolbar();
        initInstance();
        initRecycler();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.toolbar));
    }

    private void initInstance() {
        fab.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, CreateAlarmActivity.class);
            startActivityForResult(intent, REQUEST_CODE_ADD);
        });
    }

    private void initRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        recyclerListAlarm.setLayoutManager(layoutManager);
        alarmListAdapter = new AlarmListAdapter();
        recyclerListAlarm.setAdapter(alarmListAdapter);
        alarmListAdapter.setListener(new AlarmListAdapter.OnAlarmItemListener() {
            @Override
            public void onItemClick(AlarmModel alarmModel) {
                Intent intent = new Intent(mContext, CreateAlarmActivity.class);
                intent.putExtra(Constant.ALARM_MODEL, alarmModel);
                startActivityForResult(intent, REQUEST_CODE_EDIT);
            }

            @Override
            public void onItemLongClick(int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("ลบการแจ้งเตือนนี้?")
                        .setMessage("คุณแน่ใจว่าต้องการลบ?")
                        .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                            Constant.cancelAlarm(mContext, alarmModels.get(position));
                            deleteAlarm(alarmModels.get(position));
                            alarmListAdapter.deleteItem(position);
                        })
                        .setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }

            @Override
            public void onAlarmClick(AlarmModel alarmModel) {
                if (!alarmModel.isKeepAlert) {
                    Constant.cancelAlarm(mContext, alarmModel);
                    updateData(alarmModel);
                } else {
                    openAlarm(alarmModel);
                }
            }
        });
        queryListAlarm();
    }

    private void openAlarm(AlarmModel alarmModel) {
        if (alarmModel.repeatDays.size() == 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(alarmModel.alertTime);
            Calendar current = Calendar.getInstance();
            current.setTime(new Date());
            current.add(Calendar.DAY_OF_WEEK, 1);
            current.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
            current.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
            current.set(Calendar.SECOND, 0);
            current.set(Calendar.MILLISECOND, 0);
            alarmModel.alertTime = current.getTimeInMillis();
            alarmModel.dateAlert = current.getTimeInMillis();
            Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
            intent.putExtra(Constant.ALARM_ID, alarmModel.id);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), alarmModel.id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmModel.alertTime, pendingIntent);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmModel.alertTime, pendingIntent);
                }
            }
        } else {
            repeatAlarm(alarmModel);
        }
        updateData(alarmModel);
    }

    private void repeatAlarm(AlarmModel alarmModel) {
        Calendar alertTime = Calendar.getInstance();
        alertTime.setTimeInMillis(alarmModel.alertTime);
        if (alarmModel.repeatDays.size() != 7) {
            for (int i = 0; i < alarmModel.repeatDays.size(); i++) {
                if (alarmModel.repeatDays.get(i) == Calendar.SUNDAY)
                    forDay(alertTime, Calendar.SUNDAY, alarmModel.id, false);
                if (alarmModel.repeatDays.get(i) == Calendar.MONDAY)
                    forDay(alertTime, Calendar.MONDAY, alarmModel.id, false);
                if (alarmModel.repeatDays.get(i) == Calendar.TUESDAY)
                    forDay(alertTime, Calendar.TUESDAY, alarmModel.id, false);
                if (alarmModel.repeatDays.get(i) == Calendar.WEDNESDAY)
                    forDay(alertTime, Calendar.WEDNESDAY, alarmModel.id, false);
                if (alarmModel.repeatDays.get(i) == Calendar.THURSDAY)
                    forDay(alertTime, Calendar.THURSDAY, alarmModel.id, false);
                if (alarmModel.repeatDays.get(i) == Calendar.FRIDAY)
                    forDay(alertTime, Calendar.FRIDAY, alarmModel.id, false);
                if (alarmModel.repeatDays.get(i) == Calendar.SATURDAY)
                    forDay(alertTime, Calendar.SATURDAY, alarmModel.id, false);
            }
        } else {
            forDay(alertTime, alertTime.get(Calendar.DAY_OF_WEEK), alarmModel.id, true);
        }
    }

    public void forDay(Calendar alertTime, int week, int _id, boolean isWeek) {
        alertTime.set(Calendar.DAY_OF_WEEK, week);
        alertTime.set(Calendar.HOUR_OF_DAY, alertTime.get(Calendar.HOUR_OF_DAY));
        alertTime.set(Calendar.MINUTE, alertTime.get(Calendar.MINUTE));
        alertTime.set(Calendar.SECOND, 0);
        alertTime.set(Calendar.MILLISECOND, 0);
        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        intent.putExtra(Constant.ALARM_ID, _id);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), _id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            if (isWeek)
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alertTime.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pendingIntent);
            else {
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (alertTime.get(Calendar.DAY_OF_WEEK) == today.get(Calendar.DAY_OF_WEEK)) {
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        // Do something after 5ms = 500ms
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alertTime.getTimeInMillis(), pendingIntent);
                        }
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alertTime.getTimeInMillis(), 24 * 60 * 60 * 1000, pendingIntent);
                    }, 500);
                } else {
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alertTime.getTimeInMillis(), 24 * 60 * 60 * 1000, pendingIntent);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ADD && resultCode == RESULT_OK) {
            assert data != null;
            if (data.getParcelableExtra(Constant.ALARM_MODEL) != null) {
                AlarmModel alarmModel = data.getParcelableExtra(Constant.ALARM_MODEL);
                alarmModels.add(alarmModel);
                alarmListAdapter.addItem(alarmModel);
            }
        }
        if (requestCode == REQUEST_CODE_EDIT && resultCode == RESULT_OK) {
            assert data != null;
            if (data.getParcelableExtra(Constant.ALARM_MODEL) != null) {
                AlarmModel alarmModel = data.getParcelableExtra(Constant.ALARM_MODEL);
                int i = 0;
                for (AlarmModel alarmModel1 : alarmModels) {
                    if (alarmModel1.id == alarmModel.id) {
                        alarmModels.set(i, alarmModel);
                        alarmListAdapter.updateItem(i, alarmModel);
                        break;
                    } else i++;
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void queryListAlarm() {
        new AsyncTask<Void, Void, ArrayList<AlarmModel>>() {
            @Override
            protected ArrayList<AlarmModel> doInBackground(Void... voids) {
                return new ArrayList<>(Arrays.asList(DBHelper.getInstance(mContext).alarmDao().queryAllAlarm()));
            }

            @Override
            protected void onPostExecute(ArrayList<AlarmModel> alarmModelArrayList) {
                super.onPostExecute(alarmModelArrayList);
                Collections.sort(alarmModelArrayList, (o1, o2) -> Integer.compare(o1.id, o2.id));
                alarmModels.addAll(alarmModelArrayList);
                alarmListAdapter.setAlarmModels(alarmModelArrayList);
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void deleteAlarm(AlarmModel alarmModel) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                fab.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                DBHelper.getInstance(mContext).alarmDao().deleteAlarm(alarmModel);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                fab.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void updateData(AlarmModel savedAlarmModel) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                fab.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                DBHelper.getInstance(mContext).alarmDao().updateAlarm(savedAlarmModel);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                fab.setEnabled(true);
                progressBar.setVisibility(View.GONE);

                int i = 0;
                for (AlarmModel alarmModel1 : alarmModels) {
                    if (alarmModel1.id == savedAlarmModel.id) {
                        alarmModels.set(i, savedAlarmModel);
                        alarmListAdapter.updateItem(i, savedAlarmModel);
                        break;
                    } else i++;
                }
            }
        }.execute();

    }
}
