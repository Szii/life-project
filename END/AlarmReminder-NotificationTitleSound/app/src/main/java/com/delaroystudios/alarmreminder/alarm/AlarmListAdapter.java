package com.delaroystudios.alarmreminder.alarm;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delaroystudios.alarmreminder.R;

import java.util.ArrayList;

/**
 * Created by Me on 12/9/2018 AD.
 */
public class AlarmListAdapter extends RecyclerView.Adapter<AlarmItemViewHolder> {
    private OnAlarmItemListener listener;
    private ArrayList<AlarmModel> alarmModels = new ArrayList<>();

    @NonNull
    @Override
    public AlarmItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alarm_viewholder, parent, false);
        return new AlarmItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmItemViewHolder alarmItemViewHolder, int position) {
        alarmItemViewHolder.bindView(listener, alarmModels.get(position));
    }

    @Override
    public int getItemCount() {
        return alarmModels.size();
    }

    public void setListener(OnAlarmItemListener listener) {
        this.listener = listener;
    }

    public void setAlarmModels(ArrayList<AlarmModel> alarmModels) {
        this.alarmModels.addAll(alarmModels);
        notifyDataSetChanged();
    }

    public void addItem(AlarmModel alarmModel) {
        alarmModels.add(alarmModel);
        notifyItemInserted(getItemCount());
    }

    public void deleteItem(int position) {
        alarmModels.remove(position);
        notifyDataSetChanged();
    }

    public void updateItem(int position, AlarmModel alarmModel) {
        alarmModels.set(position, alarmModel);
        notifyItemChanged(position);
    }

    interface OnAlarmItemListener {
        void onItemClick(AlarmModel alarmModel);

        void onItemLongClick(int position);

        void onAlarmClick(AlarmModel alarmModel);
    }
}
